idList = "5e7097c1a0f4140f28bd40f8";
key = "1978cc65696373773108ab6e06434a13";
token = "b4e484c0a70d1aad0092e64db83049538c814c204e00910b25c6bad0bddaa568";


async function createListItem(idList,key,token,name) {
  const item = await fetch(
    `https://api.trello.com/1/cards?name=${name}&idList=${idList}&keepFromSource=all&key=${key}&token=${token}`,
    { method: 'POST' }  
  );
  const listData = await item.json();
  return listData;
}
async function getListItem(idList, key, token){
const item = await fetch(
    `https://api.trello.com/1/lists/${idList}/cards?key=${key}&token=${token}`, { method: 'GET' }
  );
var demo = {};
var listData = await item.json();
var len = listData.length;
for (i = 0;i < len; i++)
{
    demo[listData[i].id] = listData[i].name;
}
for(let x in demo){
  var node = document.createElement("li");
  var textnode = document.createTextNode(demo[x]);
  node.appendChild(textnode);
  document.getElementById("myUL").appendChild(node);
}
var myNodelist = document.getElementsByTagName("LI");
var myNodelistUpdate = document.getElementById("LI");
for (let i = 0; i < myNodelist.length; i++) {
var span = document.createElement("SPAN");
var updateSpan = document.createElement("SPAN");
var txt = document.createTextNode("\u00D7");
var txtUpdate = document.createTextNode("\u270E");
span.className = "close";
updateSpan.className = "update";
span.appendChild(txt);
updateSpan.appendChild(txtUpdate);
myNodelist[i].appendChild(span);
myNodelist[i].appendChild(updateSpan)
}
// Click on a close button to hide the current list item
var close = document.getElementsByClassName("close");
var update =  document.getElementsByClassName("update");
for (let i = 0; i < close.length; i++) {
close[i].onclick = function() {
  var div = this.parentElement;
  var text = div.textContent;
  var textNew = text.substring(0, text.length-2);
  div.style.display = "none";
  for(let [cardid, value] of Object.entries(demo)){
    if (value === textNew){
      delListItem(cardid,key,token)
    }
  }
}
}
for (let i = 0; i < update.length; i++) {
update[i].onclick = function() {
  var div = this.parentElement;
  var textUpdate = div.textContent;
  textUpdate = textUpdate.substring(0, textUpdate.length-2);
  var input = prompt("Enter new card title: ");
  if(input===null){
    input = "default value";
  }
  for(let [cardid, value] of Object.entries(demo)){
    if (value === textUpdate){
      updateList(cardid,key,token,input)
}
  }
}
}
return demo
}
async function delListItem(cardid, key, token){
const item = await fetch(
      `https://api.trello.com/1/cards/${cardid}?key=${key}&token=${token}`, { method: 'DELETE' }  
      );
const listData = await item.json();
return listData;
}
async function updateList(cardid, key, token, name){
const item = await fetch(
  `https://api.trello.com/1/cards/${cardid}?name=${name}&key=${key}&token=${token}`, { method: 'PUT' }
);
window.location.reload();
const listData = await item.json();
return listData;
}
getListItem(idList, key, token);
// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
if (ev.target.tagName === 'LI') {
  ev.target.classList.toggle('checked');
}
}, false);
// Create a new list item when clicking on the "Add" button
function newElement() {
var li = document.createElement("li");
var inputValue = document.getElementById("myInput").value;
var t = document.createTextNode(inputValue);
li.appendChild(t);
createListItem(idList,key,token,inputValue);
window.location.reload();
if (inputValue === '') {
  alert("You must write something!");
} else {
  document.getElementById("myUL").appendChild(li);
}
document.getElementById("myInput").value = "";
var span = document.createElement("SPAN");
var txt = document.createTextNode("\u00D7");
span.className = "close";
span.appendChild(txt);
li.appendChild(span);
for (i = 0; i < close.length; i++) {
  close[i].onclick = function() {
    var div = this.parentElement;
    div.style.display = "none";
  }
}
}